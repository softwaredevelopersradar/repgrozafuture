﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace GrozaFuture
{
    class DetectionPPRCHConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return System.Convert.ToBoolean((byte)value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return System.Convert.ToByte(value);
        }
    }
}
