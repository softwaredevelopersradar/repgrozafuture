﻿using System;
using System.Globalization;
using System.Windows.Data;
using ModelsTablesDBLib;

namespace GrozaFuture
{
    public class TypeRadioReconConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return System.Convert.ToBoolean((byte)value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (EnumTypeRadioRecon)System.Convert.ToInt32(value);
        }
    }
}
