﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using ModelsTablesDBLib;

namespace GrozaFuture
{
    class TypeRadioSupprConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ((EnumTypeRadioSuppr)value == (EnumTypeRadioSuppr)parameter) currentType = (EnumTypeRadioSuppr)value;
            return (EnumTypeRadioSuppr)parameter == (EnumTypeRadioSuppr)value ? true : false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ((bool)value == false) return currentType;
            currentType = (EnumTypeRadioSuppr)parameter;
            return (EnumTypeRadioSuppr)System.Convert.ToInt32(parameter);
        }

        static EnumTypeRadioSuppr currentType;
    }
}
