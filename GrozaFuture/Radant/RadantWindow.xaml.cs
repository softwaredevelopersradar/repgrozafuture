﻿using System;
using System.Windows;
using System.Windows.Input;

namespace GrozaFuture
{
    /// <summary>
    /// Логика взаимодействия для Radant.xaml
    /// </summary>
    public partial class RadantWindow : Window
    {
        public CRadantBRD.RadantView RadantObj
        {
            get=> BRDRadantGr;
            set
            {
                if (BRDRadantGr == value) return;
                BRDRadantGr = value;
            }
        }

        public event EventHandler NeedHide = (sender, obj) => { };

        public RadantWindow()
        {
            InitializeComponent();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
            this.Visibility = Visibility.Hidden;
        }

        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonDown(e);

            // Begin dragging the window
            this.DragMove();
        }
        
        private void Close_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            NeedHide(this, null);
        }
    }
}
