﻿using CRadantBRD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace GrozaFuture
{
    public partial class MainWindow
    {
        private const string RRS1 = "RRS1";
        private const string RRS2 = "RRS2";
        private const string LPA = "LPA";
        private const string LPAU = "LPAU"; // управляемый

        private ComParam standartParam = new ComParam
        {
            Parity = System.IO.Ports.Parity.None,
            DataBits = 8,
            StopBits = System.IO.Ports.StopBits.One
        };

        private RadantWindow radantWindow;

        Dictionary<string, WPFControlConnection.ConnectionControl> ARDControls;
        Dictionary<string, bool> IsConnectRadant = new Dictionary<string, bool>
        {
            {RRS1, false },
            {RRS2, false },
            {LPAU, false }
        };

        public object Regular { get; private set; }

        private void InitRadant()
        {
            radantWindow = new RadantWindow();
            radantWindow.NeedHide += RadantWindow_NeedHide;

            radantWindow.RadantObj.Radants = new System.Collections.ObjectModel.ObservableCollection<RadantModel>
            {
                new RadantModel
                {
                    NameDevice = "RRS1",
                    Name = "RRC 1",
                    Course = 0,
                    DColor = (Brush)new System.Windows.Media.BrushConverter().ConvertFromString("#0000FF"),
                    Correct = 270,
                    Tier = 0,

                },

                new RadantModel
                {
                    NameDevice = "RRS2",
                    Name = "RRC 2",
                    Course = 79,
                    DColor = (Brush)new System.Windows.Media.BrushConverter().ConvertFromString("#0E70FF"),
                    Correct = 0,
                    Tier = 1
                } ,

                new RadantModel
                {
                    NameDevice = LPA,
                    Name = "LPA 1,3",
                    Course = 321,
                    DColor = (Brush)new System.Windows.Media.BrushConverter().ConvertFromString("#6b8e23"),
                    Correct = 0,
                    Tier = 0
                } ,

                new RadantModel
                {
                    NameDevice = LPA,
                    Name = "LPA 2,4",
                    Course = 56,
                    DColor = (Brush)new System.Windows.Media.BrushConverter().ConvertFromString("#228b22"),
                    Correct = 0,
                    Tier = 0
                } ,

                new RadantModel
                {
                    NameDevice = LPAU,
                    Name = "LPA 5-10",
                    Course = 0,
                    DColor = (Brush)new System.Windows.Media.BrushConverter().ConvertFromString("#9ACD32"),
                    Correct = 180,
                    Tier = 0
                } ,
            };

            ARDControls = new Dictionary<string, WPFControlConnection.ConnectionControl>
            {
                { RRS1, ARD1Connection },
                { RRS2, ARD2Connection },
                { LPAU, ARD3Connection }
            };

            radantWindow.RadantObj.OnClosePort += Radant_OnClosePort;
            radantWindow.RadantObj.OnOpenPort += Radant_OnOpenPort;
            radantWindow.RadantObj.OnReadByte += Radant_OnReadByte;
            radantWindow.RadantObj.OnWriteByte += Radant_OnWriteByte;
        }

        private void RadantWindow_NeedHide(object sender, EventArgs e)
        {
            OButton.IsChecked = false;
        }

        private void Radant_Click(object sender, RoutedEventArgs e)
        {
            if (radantWindow.IsVisible)
                radantWindow.Hide();
            else
                radantWindow.Show();
        }

        #region HandlerEvents

        private void Radant_OnWriteByte(object sender, CRadantBRD.Events.ByteEventArgs e)
        {
            this.Dispatcher.Invoke((Action)(() =>
            {
                RadantModel radant = sender as RadantModel;
                ARDControls[radant.NameDevice].ShowWrite();
            }));
        }

        private void Radant_OnReadByte(object sender, CRadantBRD.Events.ByteEventArgs e)
        {
            this.Dispatcher.Invoke((Action)(() =>
            {
                RadantModel radant = sender as RadantModel;
                ARDControls[radant.NameDevice].ShowRead();
            }));
        }

        private void Radant_OnOpenPort(object sender, EventArgs e)
        {
            this.Dispatcher.Invoke((Action)(() =>
            {
                RadantModel radant = sender as RadantModel;
                IsConnectRadant[radant.NameDevice] = true;
                ARDControls[radant.NameDevice].ShowConnect();
            }));
        }

        private void Radant_OnClosePort(object sender, EventArgs e)
        {
            this.Dispatcher.Invoke((Action)(() =>
            {
                RadantModel radant = sender as RadantModel;
                IsConnectRadant[radant.NameDevice] = false;
                ARDControls[radant.NameDevice].ShowDisconnect();
            }));
        }

        #endregion

        private void ARD1Connection_ButServerClick(object sender, RoutedEventArgs e)
        {
            if (IsConnectRadant[RRS1])
                radantWindow.RadantObj.Radants.First(radant => radant.NameDevice == RRS1).ClosePort();
            else
                radantWindow.RadantObj.Radants.First(radant => radant.NameDevice == RRS1).OpenPort(new ComParam(basicProperties.Local.ARD1.ComPort, basicProperties.Local.ARD1.PortSpeed, standartParam.Parity, standartParam.DataBits, standartParam.StopBits));
        }

        private void ARD2Connection_ButServerClick(object sender, RoutedEventArgs e)
        {
            if (IsConnectRadant[RRS2])
                radantWindow.RadantObj.Radants.First(radant => radant.NameDevice == RRS2).ClosePort();
            else
                radantWindow.RadantObj.Radants.First(radant => radant.NameDevice == RRS2).OpenPort(new ComParam(basicProperties.Local.ARD2.ComPort, basicProperties.Local.ARD1.PortSpeed, standartParam.Parity, standartParam.DataBits, standartParam.StopBits));
        }

        private void ARD3Connection_ButServerClick(object sender, RoutedEventArgs e)
        {
            if (IsConnectRadant[LPAU])
                radantWindow.RadantObj.Radants.First(radant => radant.NameDevice == LPAU).ClosePort();
            else
                radantWindow.RadantObj.Radants.First(radant => radant.NameDevice == LPAU).OpenPort(new ComParam(basicProperties.Local.ARD3.ComPort, basicProperties.Local.ARD1.PortSpeed, standartParam.Parity, standartParam.DataBits, standartParam.StopBits));
        }

    }
}
