﻿using ModelsTablesDBLib;
using System;
using System.Collections.Generic;
using System.Windows;
using UserControl_Chat;

namespace GrozaFuture
{
    public partial class MainWindow
    {
        private Chat newWindow;
        private Buble chatBuble;

        private void InitChat()
        {
            newWindow = new Chat();
            chatBuble = new Buble();
            newWindow.SetStations();
            Events.OnClosingChat += DeactivatChatButton;
        }

        private void UpdateSideMenu(List<TableASP> ASPList)
        {
            newWindow.UpdateSideMenu(ASPList);
        }


        private void ChatButton_Click(object sender, RoutedEventArgs e)
        {
            if (newWindow.IsVisible)
                newWindow.Hide();
            else
                newWindow.Show();

        }

        private void DeactivatChatButton()
        {
            ChatButton.IsChecked = false;
        }

        private void MainWindow_Closed(object sender, EventArgs e)
        {
            System.Windows.Threading.Dispatcher.ExitAllFrames();
            //newWindow.Close();
        }


    }
}
