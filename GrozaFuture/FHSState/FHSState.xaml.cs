﻿using System;
using System.Windows;
using System.Windows.Input;
using FPSFullStateControl;
using FpsStateControl;
using System.Collections.Generic;
using ModelsTablesDBLib;

using System.Threading;

namespace GrozaFuture.FHSState
{
    /// <summary>
    /// Логика взаимодействия для FHSState.xaml
    /// </summary>
    public partial class FHSState : Window
    {
        public FHSState()
        {
            InitializeComponent();
            quickFPS.numOfLetters = 9;
            fullFPS.numOfLetters = 9;
        }

        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonDown(e);
            this.DragMove();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
            this.Visibility = Visibility.Hidden;
        }

        private void HideWindow()
        {
            this.Visibility = Visibility.Hidden;
        }

        public void UpdateState(byte[] data)
        {
            try {
                byte[] state = new byte[data.Length/2];
                short[] halfPow = new short[data.Length / 2];
                for (int i = 0; i < data.Length / 2; i++)
                {
                    state[i] = data[i * 2];
                    halfPow[i] = data[i * 2 + 1];
                }
                Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                { quickFPS.UpdateStates(state);
                });
                UpdateHalfPower(halfPow);
            }

            catch (Exception)
            { }
        }
        public void UpdateStateFake(byte[] data)
        {
            try
            {
                Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                {
                    quickFPS.UpdateStatesFake(data);
                });
            }

            catch (Exception)
            { }
        }
        public void ClearFake()
        {
            try
            {
                Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                {
                    quickFPS.ClearTable();
                });
            }

            catch (Exception)
            { }
        }

        public void UpdateHalfPower(short[] powCodes)
        {
            try {
                Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                { quickFPS.UpdateHalfPow(powCodes); 
                });
            }
            catch (Exception)
            { }
        }

        public void UpdatePower(short[] power, short literCount)
        {
            try {
                Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                {
                    fullFPS.UpdatePower(power, literCount);
            });
        }
            catch (Exception)
            { }
        }
        public void UpdateVoltage(float[] voltage, short literCount)
        {
            try {
                Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                {
                    fullFPS.UpdateVoltage(voltage, literCount);
                });
                //if ((voltage.Length == 10) && (voltage[9] != 0))
                //{
                    
                //    UpdateStateFake(new byte[1] {136 });
                //    UpdateHalfPower(new short[1] { 1 });
                //}
                //else {
                //    ClearFake();
                //    UpdateHalfPower(new short[1] { 0 });
                //}
            }
            catch (Exception)
            { }
        }
        public void UpdateCurrent(byte[] current, int literCount)
        {
            try {
                Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                {
                    fullFPS.UpdateCurrent(current, literCount);
                });
            }
            catch (Exception)
            { }
        }
        public void UpdateTemperature(byte[] temp, int literCount)
        {
            try
            {
                Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                {
                    fullFPS.UpdateTemperature(temp, literCount);
            });
        }
            catch (Exception)
            { }
        }

        public void UpdateErrors(byte[] errors, int literCount)
        {
            try
            {
                Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                {
                   quickFPS.DeleteNullStates(fullFPS.UpdateError(errors,literCount));
                });
            }
            catch (Exception)
            { }
        }

        public delegate void Event(object sender);
        public event Event NeedHide;

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                this.Hide();
                NeedHide?.Invoke(this);
            }
        }

        private void BCloseFHSState_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            NeedHide?.Invoke(this);
        }

        public void SetFHSLanguage(Languages language)
        {
            ResourceDictionary dict = new ResourceDictionary();
            try
            {
                switch (language)
                {
                    case Languages.Eng:
                        dict.Source = new Uri("/GrozaFuture;component/Languages/TranslatorLeftControls/TranslatorLeftControls.EN.xaml",
                                      UriKind.Relative);
                        break;
                    case Languages.Rus:
                        dict.Source = new Uri("/GrozaFuture;component/Languages/TranslatorLeftControls/TranslatorLeftControls.RU.xaml",
                                           UriKind.Relative);
                        break;
                    default:
                        dict.Source = new Uri("/GrozaFuture;component/Languages/TranslatorLeftControls/TranslatorLeftControls.RU.xaml",
                                          UriKind.Relative);
                        break;
                }

                this.Resources.MergedDictionaries.Add(dict);
            }
            catch (Exception ex)
            { }
        }
    }
}
