﻿using ModelsTablesDBLib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using YamlDotNet.Serialization;

namespace GrozaFuture
{
    public partial class MainWindow
    {
        private const int HeadAngDelta = 90;
        public void InitLocalProperties()
        {
            var localProperties = YamlLoad();

            try
            {
                basicProperties.Local = localProperties;
                //basicProperties.Local.Update(localProperties);
                UpdateLocalProperties4MainPanel(localProperties);
                endPoint = localProperties.DbServer.IpAddress + ":" + localProperties.DbServer.Port;
                ComPort = localProperties.ARONE.ComPort;
                BaudRate = localProperties.ARONE.PortSpeed;
                CurrentDataBank = localProperties.ARONE.CurrentDataBank;

                SetControlsLanguage();
            }
            catch { }
        }


        public void HandlerLocalProperties(object sender, LocalProperties arg)
        {
            UpdateLocalProperties4MainPanel(arg);

            endPoint = arg.DbServer.IpAddress + ":" + arg.DbServer.Port;
            ComPort = arg.ARONE.ComPort;
            BaudRate = arg.ARONE.PortSpeed;
            CurrentDataBank = arg.ARONE.CurrentDataBank;

            YamlSave(arg);
        }

        public async void HandlerGlobalProperties(object sender, GlobalProperties arg)
        {
            if(arg.SignHeadingAngle)
            {
                var tempGNSS = (await clientDB?.Tables[NameTable.TempGNSS].LoadAsync<TempGNSS>()).FirstOrDefault();
                if (tempGNSS != null)
                    arg.HeadingAngle = (int)Math.Round(tempGNSS.CmpRR) + HeadAngDelta;

            }
            clientDB?.Tables[NameTable.GlobalProperties].Add(arg);

            UpdateGlobalProperties4MainPanel(arg);
        }

        private async void HandlerOnSignHeadingAngle(object sender, bool SignHeadingAngle)
        {
        }
    }
}