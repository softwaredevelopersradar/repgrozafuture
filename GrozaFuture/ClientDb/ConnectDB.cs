﻿using System.Windows;
using ClientDataBase;
using InheritorsEventArgs;

namespace GrozaFuture
{
    public partial class MainWindow
    {
        private void DbConnection_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (clientDB != null)
                    clientDB.Disconnect();
                else
                {
                    clientDB = new ClientDB(this.Name, endPoint);
                    InitClientDB();
                    clientDB.ConnectAsync();
                }
            }
            catch (ClientDataBase.Exceptions.ExceptionClient exceptClient)
            {
                HandlerDisconnect_ClientDb(this, null);
                MessageBox.Show(exceptClient.Message);
            }
        }

        private void HandlerDisconnect_ClientDb(object sender, ClientEventArgs e)
        {
            DbControlConnection.ShowDisconnect();
            clientDB = null;
        }

        private void HandlerConnect_ClientDb(object sender, ClientEventArgs e)
        {
            DbControlConnection.ShowConnect();
            LoadTables();
        }

    }
}
