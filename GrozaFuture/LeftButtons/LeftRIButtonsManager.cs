﻿using System;
using System.Windows;

namespace GrozaFuture
{
    public partial class MainWindow
    {

        private byte ShiftTypeRadioRecon = 1;
        private byte ShiftTypeRadioSuppr = 3;

        private void InitDefaultRIButtons()
        {
        }

        private int DesiredRadioReconMode()
        {
            return (byte)basicProperties.Global.TypeRadioRecon + ShiftTypeRadioRecon;
        }

        private int DesiredRadioSupprMode()
        {
            if (basicProperties.Global.TypeRadioSuppr == ModelsTablesDBLib.EnumTypeRadioSuppr.Voice) return 5;
            return (byte)basicProperties.Global.TypeRadioSuppr + ShiftTypeRadioSuppr;
        }

        private byte DesireDetectionFHSS()
        {
            byte desireDetectionFHSS = Convert.ToByte(basicProperties.Global.DetectionFHSS);
            return (byte)((desireDetectionFHSS > 0) ? 1 : 0);
        }

        private async void BearingToggleButton_Click(object sender, RoutedEventArgs e)
        {        
            if (pLibrary.Mode == 0 || pLibrary.Mode == 1 || pLibrary.Mode == 2)
            {
                if (pLibrary.Mode == 1 || pLibrary.Mode == 2)
                {
                    var answer = await dsp.SetMode(DesiredRadioReconMode());
                    if (answer?.Header.ErrorCode == 0)
                    {
                        pLibrary.Mode = DesiredRadioReconMode();
                        AttCheck(DesiredRadioReconMode());
                    }
                }
            }
            else
            {
                BearingToggleButton.IsChecked = !BearingToggleButton.IsChecked;
            }
        }
        
        private async void SearchFHSSToggleButton_Click(object sender, RoutedEventArgs e)
        {
            if (pLibrary.Mode == 0 || pLibrary.Mode == 1 || pLibrary.Mode == 2)
            {
                byte desireDetectionFHSS = DesireDetectionFHSS();
                var answer = await dsp.SetSearchFHSS(desireDetectionFHSS);
            }
            else
            {
                SearchFHSSToggleButton.IsChecked = !SearchFHSSToggleButton.IsChecked;
            }
        }
    }
}
