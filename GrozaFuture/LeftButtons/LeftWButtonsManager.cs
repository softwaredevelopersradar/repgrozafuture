﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace GrozaFuture
{
    public partial class MainWindow
    {

        private Att att = new Att();

        private void InitAtt()
        {
            att.AttValueChange += Att_AttValueChange;
            att.NeedHide += Att_NeedHide;
            att.NeedGetRequest += Att_NeedGetRequest;
        }

        private void Att_NeedHide(object sender)
        {
            AToggleButton.IsChecked = false;
        }

        private async void Att_AttValueChange(object sender, int BandBumber, double AttValue)
        {
            bool IsConstAttenuatorEnabled = false;
            if (AttValue < 10)
            {
                IsConstAttenuatorEnabled = false;
            }
            if (AttValue == 10)
            {
                AttValue = 0;
                IsConstAttenuatorEnabled = true;
            }
            if (AttValue > 10)
            {
                AttValue = AttValue - 10;
                IsConstAttenuatorEnabled = true;
            }
            var asnwer = await dsp.SetAttenuatorsValue(BandBumber, (float)AttValue, IsConstAttenuatorEnabled);
        }

        private async void PLibrary_NbarIndex(object sender, int index)
        {
            if (att.IsVisible)
            {
                var answer1 = await dsp.GetAmplifierValues(index);
                float fAmplevel = 0;
                if (answer1?.Header.ErrorCode == 0)
                {
                    int Amplevel = answer1.Settings[0];
                    fAmplevel = Amplevel / 2.0f;
                }
                var answer2 = await dsp.GetAttenuatorsValues(index);
                float fAttlevel = 0;
                if (answer2?.Header.ErrorCode == 0)
                {
                    var Attlevel = answer2.Settings[0].AttenuatorValue;
                    fAttlevel = Attlevel / 2.0f;
                    var cAtt = Convert.ToBoolean(answer2.Settings[0].IsConstAttenuatorEnabled);
                    fAttlevel += (cAtt) ? 10 : 0;
                }
                att.InitValues(index, fAttlevel, fAmplevel);
            }
        }

        private async void Att_NeedGetRequest(object sender, int BandBumber)
        {
            if (att.IsVisible)
            {
                var answer1 = await dsp.GetAmplifierValues(BandBumber);
                float fAmplevel = 0;
                if (answer1?.Header.ErrorCode == 0)
                {
                    int Amplevel = answer1.Settings[0];
                    fAmplevel = Amplevel / 2.0f;
                }
                var answer2 = await dsp.GetAttenuatorsValues(BandBumber);
                float fAttlevel = 0;
                if (answer2?.Header.ErrorCode == 0)
                {
                    var Attlevel = answer2.Settings[0].AttenuatorValue;
                    fAttlevel = Attlevel / 2.0f;
                    var cAtt = Convert.ToBoolean(answer2.Settings[0].IsConstAttenuatorEnabled);
                    fAttlevel += (cAtt) ? 10 : 0;
                }
                att.InitValues(BandBumber, fAttlevel, fAmplevel);
            }
        }

        private void AToggleButton_Click(object sender, RoutedEventArgs e)
        {
            if (pLibrary.Mode != 2)
            {
                AToggleButton.IsChecked = !AToggleButton.IsChecked;
                if (basicProperties.Local.Common.Language.ToString() == "Rus")
                    MessageBox.Show("Управление аттенюаторами доступно только в режиме Радиоразведка с пеленгованием!", "Внимание!", MessageBoxButton.OK, MessageBoxImage.Warning);
                else
                    MessageBox.Show("Control of attenuators is available only in the Radio intelligence mode with direction finding!", "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

            if (AToggleButton.IsChecked.Value)
                att.Show();
            else
                att.Hide();
        }

        public void AttCheck(DspDataModel.Tasks.DspServerMode Mode)
        {
            if (att.IsVisible == true && Mode != DspDataModel.Tasks.DspServerMode.RadioIntelligenceWithDf)
            {
                AToggleButton.IsChecked = false;
                att.Hide();
            }
        }

        public void AttCheck(int Mode)
        {
            if (att.IsVisible == true && Mode != 2)
            {
                AToggleButton.IsChecked = false;
                att.Hide();
            }
        }
    }
}
