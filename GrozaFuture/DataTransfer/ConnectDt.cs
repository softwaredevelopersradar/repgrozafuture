﻿using DataTransferClientLibrary;
using ModelsTablesDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace GrozaFuture
{
    public partial class MainWindow
    {
        private void DtConnection_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (dTClient.IsConnected)
                {
                    dTClient.Disconnect();
                    EDControlConnection.ShowDisconnect();
                }
                else
                {
                    dTClient.Connect(string.Format("test ARM name: {0}", Dns.GetHostByName(Dns.GetHostName()).AddressList[0].ToString()));
                    if (dTClient.IsConnected)
                        EDControlConnection.ShowConnect();
                    else
                        EDControlConnection.ShowDisconnect();
                }
            }
            catch
            {
                dTClient.Disconnect();
                EDControlConnection.ShowDisconnect();
            }
        }

        private void HandlerDisconnect_DTClient(object sender)
        {
            EDControlConnection.ShowDisconnect();
        }

        private void HandlerConnect_DTClient(object sender)
        {
            EDControlConnection.ShowConnect();
        }
    }
}
