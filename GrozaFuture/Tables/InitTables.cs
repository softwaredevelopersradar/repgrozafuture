﻿using ModelsTablesDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TableEvents;

namespace GrozaFuture
{
    public partial class MainWindow
    {
        public void InitTables()
        {
            // Таблица АСП
            ucASP.OnAddRecord += new EventHandler<TableEvent>(OnAddRecord);
            ucASP.OnChangeRecord += new EventHandler<TableEvent>(OnChangeRecord);
            ucASP.OnDeleteRecord += new EventHandler<TableEvent>(OnDeleteRecord);
            ucASP.OnClearRecords += new EventHandler<NameTable>(OnClearRecords);
            ucASP.OnSelectedRow += new EventHandler<ASPEvents>(UcASP_OnSelectedRow);
            ucASP.OnIsWindowPropertyOpen += new EventHandler<ASPControl.ASPProperty>(UcASP_OnIsWindowPropertyOpen);

            // Таблица Запрещенныее частоты
            ucSpecFreqForbidden.OnAddRecord += new EventHandler<TableEvent>(OnAddRecord);
            ucSpecFreqForbidden.OnChangeRecord += new EventHandler<TableEvent>(OnChangeRecord);
            ucSpecFreqForbidden.OnDeleteRecord += new EventHandler<TableEvent>(OnDeleteRecord);
            //ucSpecFreqForbidden.OnClearRecords += new EventHandler<NameTable>(OnClearRecords);            
            ucSpecFreqForbidden.OnClearRecords += new EventHandler<NameTable>(OnClearRecordsByFilter);
            ucSpecFreqForbidden.OnIsWindowPropertyOpen += new EventHandler<SpecFreqControl.SpecFreqProperty>(UcSpecFreqForbidden_OnIsWindowPropertyOpen);

            // Таблица Важные частоты
            ucSpecFreqImportant.OnAddRecord += new EventHandler<TableEvent>(OnAddRecord);
            ucSpecFreqImportant.OnChangeRecord += new EventHandler<TableEvent>(OnChangeRecord);
            ucSpecFreqImportant.OnDeleteRecord += new EventHandler<TableEvent>(OnDeleteRecord);
            //ucSpecFreqImportant.OnClearRecords += new EventHandler<NameTable>(OnClearRecords);
            ucSpecFreqImportant.OnClearRecords += new EventHandler<NameTable>(OnClearRecordsByFilter);
            ucSpecFreqImportant.OnIsWindowPropertyOpen += new EventHandler<SpecFreqControl.SpecFreqProperty>(UcSpecFreqImportant_OnIsWindowPropertyOpen);

            // Таблица Известные частоты
            ucSpecFreqKnown.OnAddRecord += new EventHandler<TableEvent>(OnAddRecord);
            ucSpecFreqKnown.OnChangeRecord += new EventHandler<TableEvent>(OnChangeRecord);
            ucSpecFreqKnown.OnDeleteRecord += new EventHandler<TableEvent>(OnDeleteRecord);
            //ucSpecFreqKnown.OnClearRecords += new EventHandler<NameTable>(OnClearRecords);
            ucSpecFreqKnown.OnClearRecords += new EventHandler<NameTable>(OnClearRecordsByFilter);
            ucSpecFreqKnown.OnIsWindowPropertyOpen += new EventHandler<SpecFreqControl.SpecFreqProperty>(UcSpecFreqKnown_OnIsWindowPropertyOpen);

            // Таблица Сектора и диапазоны РР
            ucSRangesRecon.OnAddRecord += new EventHandler<TableEvent>(OnAddRecord);
            ucSRangesRecon.OnChangeRecord += new EventHandler<TableEvent>(OnChangeRecord);
            ucSRangesRecon.OnDeleteRecord += new EventHandler<TableEvent>(OnDeleteRecord);
            //ucSRangesRecon.OnClearRecords += new EventHandler<NameTable>(OnClearRecords);
            ucSRangesRecon.OnClearRecords += new EventHandler<NameTable>(OnClearRecordsByFilter);
            ucSRangesRecon.OnLoadDefaultSRanges += new EventHandler<SRangesArgs>(UcSRangesRecon_OnLoadDefaultSRanges);
            ucSRangesRecon.OnIsWindowPropertyOpen += new EventHandler<SectorsRangesControl.SectorsRangesProperty>(UcSRangesRecon_OnIsWindowPropertyOpen);

            // Таблица Сектора и диапазоны РП
            ucSRangesSuppr.OnAddRecord += new EventHandler<TableEvent>(OnAddRecord);
            ucSRangesSuppr.OnChangeRecord += new EventHandler<TableEvent>(OnChangeRecord);
            ucSRangesSuppr.OnDeleteRecord += new EventHandler<TableEvent>(OnDeleteRecord);
            //ucSRangesSuppr.OnClearRecords += new EventHandler<NameTable>(OnClearRecords);
            ucSRangesSuppr.OnClearRecords += new EventHandler<NameTable>(OnClearRecordsByFilter);
            ucSRangesSuppr.OnLoadDefaultSRanges += new EventHandler<SRangesArgs>(UcSRangesSuppr_OnLoadDefaultSRanges);
            ucSRangesSuppr.OnIsWindowPropertyOpen += new EventHandler<SectorsRangesControl.SectorsRangesProperty>(UcSRangesSuppr_OnIsWindowPropertyOpen);

            // Таблица ИРИ ФРЧ
            ucTemsFWS.OnDeleteRecord += new EventHandler<TableEvent>(UcTemsFWS_OnDeleteRecord);
            ucTemsFWS.OnClearRecords += new EventHandler<NameTable>(OnClearRecords);
            ucTemsFWS.OnAddFWS_RS += new EventHandler<TableSuppressFWS>(UcTemsFWS_OnAddFWS_RS);
            ucTemsFWS.OnAddFWS_TD += new EventHandler<TableReconFWS>(UcTemsFWS_OnAddFWS_TD);
            ucTemsFWS.OnSelectedRow += new EventHandler<TableEvent>(UcTemsFWS_OnSelectedRow);
            ucTemsFWS.OnGetExecBear += new EventHandler<TempFWS>(UcTemsFWS_OnGetExecBear);
            ucTemsFWS.OnGetKvBear += new EventHandler<TempFWS>(UcTemsFWS_OnGetKvBear);
            ucTemsFWS.OnSendFreqCRRD += new EventHandler<TempFWS>(UcTemsFWS_OnSendFreqCRRD);

            // Таблица ИРИ ФРЧ РП
            ucSuppressFWS.OnAddRecord += new EventHandler<TableEvent>(OnAddRecord);
            ucSuppressFWS.OnChangeRecord += new EventHandler<TableEvent>(OnChangeRecord);
            ucSuppressFWS.OnDeleteRecord += new EventHandler<TableEvent>(OnDeleteRecord);
            //ucSuppressFWS.OnClearRecords += new EventHandler<NameTable>(OnClearRecords);
            ucSuppressFWS.OnClearRecords += new EventHandler<NameTable>(OnClearRecordsByFilter);
            ucSuppressFWS.OnDeleteRange += new EventHandler<List<TableSuppressFWS>>(UcSuppressFWS_OnDeleteRange);
            ucSuppressFWS.OnGetExecBear += new EventHandler<TableSuppressFWS>(UcSuppressFWS_OnGetExecBear);
            ucSuppressFWS.OnSendFreqCRRD += new EventHandler<TempFWS>(UcSuppressFWS_OnSendFreqCRRD);

            // Таблица ИРИ ФРЧ ЦР
            ucReconFWS.OnDeleteRecord += new EventHandler<TableEvent>(OnDeleteRecord);
            ucReconFWS.OnClearRecords += new EventHandler<NameTable>(OnClearRecords);
            ucReconFWS.OnAddFWS_RS += new EventHandler<TableSuppressFWS>(UcReconFWS_OnAddFWS_RS);
            ucReconFWS.OnGetExecBear += new EventHandler<TableReconFWS>(UcReconFWS_OnGetExecBear);
            ucReconFWS.OnGetKvBear += new EventHandler<TableReconFWS>(UcReconFWS_OnGetKvBear);
            ucReconFWS.OnSendFreqCRRD += new EventHandler<TempFWS>(UcReconFWS_OnSendFreqCRRD);
            ucReconFWS.OnClickTDistribution += new EventHandler(UcReconFWS_OnClickTDistribution);
            ucReconFWS.OnSendFWS_TD_RS += new EventHandler<List<TableSuppressFWS>>(UcReconFWS_OnSendFWS_TD_RS);
            ucReconFWS.OnClickUS += new EventHandler<bool>(UcReconFWS_OnClickUS);
            ucReconFWS.OnClickRS += new EventHandler<bool>(UcReconFWS_OnClickRS);

            // Таблица ИРИ ППРЧ
            ucReconFHSS.OnDeleteRecord += new EventHandler<TableEvent>(OnDeleteRecord);
            ucReconFHSS.OnClearRecords += new EventHandler<NameTable>(OnClearRecords);
            ucReconFHSS.OnSelectedRow += new EventHandler<TableEvent>(UcReconFHSS_OnSelectedRow);
            ucReconFHSS.OnAddFHSS_RS_Recon += new EventHandler<TableSuppressFHSS>(UcReconFHSS_OnAddFHSS_RS_Recon);

            // Таблица ИРИ ППРЧ РП
            ucSuppressFHSS.OnAddRecord += new EventHandler<TableEvent>(OnAddRecord);
            ucSuppressFHSS.OnChangeRecord += new EventHandler<TableEvent>(OnChangeRecord);
            ucSuppressFHSS.OnDeleteRecord += new EventHandler<TableEvent>(OnDeleteRecord);
            ucSuppressFHSS.OnClearRecords += new EventHandler<NameTable>(OnClearRecords);
           
        }

      
    }
}
