﻿using DLL_ADSB_Library;
using ModelsTablesDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows;
using TempADSBLibrary;

namespace GrozaFuture
{
    public partial class MainWindow
    {
        ADSB_Decoder ADSBDecoder;
        bool connected;

        TimerCallback tm;
        Timer timer;

        Dictionary<string, bool> curPlaneCollection;
        Dictionary<string, DateTime> planeLifeTimeCollection;
        DateTime curDateStep;

        private void ADSBConnection_Click(object sender, RoutedEventArgs e)
        {
            TempEvents.OnIsConnected += ChangeConnectionStatus;
            try
            {
                if (connected != false)
                {
                    //ADSBDecoder.Disconnect();
                    DataDecoder.CloseConnection();
                    connected = false;
                    tm = null;
                    timer = null;
                }
                else
                {
                    //Task.Run(() => ADSBDecoder.ConnectToADSB());
                    //Events.OnReceivePlaneData += AddPlaneToTable;
                    //Events.OnReceiveShortPlaneData += AddPlaneWithShortDataToTable;
                    //var localProperties = YamlLoad<LocalProperties>("LocalProperties.yaml");
                    //DataDecoder.ConnectADSB(localProperties.ADSB.IpAddress, localProperties.ADSB.Port);
                    DataDecoder.ConnectADSB(basicProperties.Local.ADSB.IpAddress, basicProperties.Local.ADSB.Port);
                    TempEvents.OnReceivePlaneData += AddPlaneToTable;
                    TempEvents.OnReceiveShortPlaneData += AddPlaneWithShortDataToTable;
                    curPlaneCollection = new Dictionary<string, bool>();
                    planeLifeTimeCollection = new Dictionary<string, DateTime>();
                    connected = true;
                    //tm = new TimerCallback(TestRelevanceOfPlanes);
                    //timer = new Timer(tm, 0, 900000, 900000);
                    curDateStep = DateTime.Now;
                }
            }
            catch (Exception exp)
            {
                ADSBControlConnection.ShowDisconnect();
                MessageBox.Show(exp.Message);
            }

        }

        private void ChangeConnectionStatus(bool connected)
        {
            if (connected)
                ADSBControlConnection.ShowConnect();
            else
                ADSBControlConnection.ShowDisconnect();
        }

        private bool ValidateADSBProperties(LocalProperties localProperties)
        {
            if (localProperties.ADSB.IpAddress.Equals("192.168.0.11") && localProperties.ADSB.Port == 30005)
                return true;
            else
                return false;
        }



        private void AddPlaneWithShortDataToTable(string iCAO, double altitude)
        {
            try
            {
                if (clientDB != null)
                {
                    if (curPlaneCollection.Keys.Contains(iCAO) && curPlaneCollection[iCAO] == false)
                    {
                        planeLifeTimeCollection[iCAO] =  DateTime.Now;
                        AddPlaneToBD(iCAO, altitude);
                    }

                    if (!curPlaneCollection[iCAO])
                    {
                        AddPlaneToBD(iCAO, altitude);
                        planeLifeTimeCollection.Add(iCAO, DateTime.Now);
                        curPlaneCollection.Add(iCAO, false);
                    }
                }
            }
            catch
            { }

        }

        private void AddPlaneToBD(string iCAO, double altitude)
        {
            clientDB.Tables[NameTable.TempADSB].Add(new TempADSB()
            {
                Id = iCAO.GetHashCode(),
                ICAO = iCAO,
                Coordinates = new Coord()
                {
                    Altitude = altitude,
                }
            });
        }

        private void AddPlaneToTable(string iCAO, double latitude, double longitude, double altitude)
        {
            try
            {
                if (clientDB != null)
                {
                    if (clientDB.IsConnected())
                    {
                        clientDB.Tables[NameTable.TempADSB].Add(new TempADSB()
                        {
                            ICAO = iCAO,
                            Id = iCAO.GetHashCode(),
                            Coordinates = new Coord()
                            {
                                Latitude = latitude,
                                Longitude = longitude,
                                Altitude = altitude
                            }
                        });
                    }
                }
                if (!curPlaneCollection.Keys.Contains(iCAO))
                {
                    planeLifeTimeCollection.Add(iCAO, DateTime.Now);
                    curPlaneCollection.Add(iCAO, true);
                }
                if (curPlaneCollection.Keys.Contains(iCAO) || curPlaneCollection[iCAO] == false)
                {
                    planeLifeTimeCollection[iCAO] = DateTime.Now;
                    curPlaneCollection[iCAO] = true;
                }
            }
            catch
            { }
        }

        protected void TestRelevanceOfPlanes(object obj)
        {
            //try
            //{
            //    foreach (string ICAO in planeLifeTimeCollection.Keys)
            //    {
            //        if(curDateStep >= planeLifeTimeCollection[ICAO])
            //        {
            //            clientDB.Tables[NameTable.TempADSB].Delete(new TempADSB()
            //            {
            //                ICAO = ICAO,
            //            });
            //        }
            //    }
            //    // planeLifeTimeCollection.Where(x => x.Value);

            //    curDateStep = DateTime.Now;
            //}
            //catch { }
           
        }


    }
}
