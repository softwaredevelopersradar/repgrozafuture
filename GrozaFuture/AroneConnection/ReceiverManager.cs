﻿using ModelsTablesDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfControlLibraryBut;

namespace GrozaFuture
{
    public partial class MainWindow
    {
        public void InitReceiverManager()
        {            
            ReceiverTable.OnFreqSupress += new EventHandler<ToSupressEventArgs>(DataToTable);            
        }

        public void DataToTable(object sender, ToSupressEventArgs e)
        {
            var Id = e.ID;
            switch (Id)
            {
                case 0:
                    TableSuppressFWS tableSuppressFWS = new TableSuppressFWS
                    {
                        Id = 0,
                        Sender = SignSender.RadioRecevier,
                        NumberASP = TableEvents.PropNumberASP.SelectedNumASP,
                        FreqKHz = e.FrqSupress / 1000,
                        Bearing = -1,
                        Letter = TableOperations.DefinitionParams.DefineLetter(e.FrqSupress / 1000),
                        Threshold = -1,
                        Priority = 2,
                        Coordinates = new Coord
                        {
                            Latitude = -1,
                            Longitude = -1,
                            Altitude = -1
                        },
                        InterferenceParam = new InterferenceParam
                        {
                            Manipulation = 0,
                            Modulation = 1,
                            Deviation = 8,
                            Duration = 4
                        },
                    };

                    UcTemsFWS_OnAddFWS_RS(this, tableSuppressFWS);
                    break;                    
                case 1:
                    TableReconFWS tableReconFWS = new TableReconFWS
                    {
                        Id = 0,
                        FreqKHz = e.FrqSupress / 1000,
                        Time = DateTime.Now.ToLocalTime(),
                        Deviation = (float)(e.BwSupress * 1000.0),
                        Coordinates = new Coord
                        {
                            Latitude = -1,
                            Longitude = -1,
                            Altitude = -1
                        },
                        ListJamDirect = new System.Collections.ObjectModel.ObservableCollection<TableJamDirect>
                        {
                            new TableJamDirect
                            {
                                JamDirect = new JamDirect
                                {
                                    NumberASP = TableEvents.PropNumberASP.SelectedNumASP,
                                    Bearing = -1,
                                    Level = -1,
                                    Std = -1,
                                    DistanceKM = -1,
                                    IsOwn = true
                                }
                            }
                        }
                    };

                    UcTemsFWS_OnAddFWS_TD(this, tableReconFWS);
                    break;
                case 2:
                    double freqMHz = e.FrqSupress / 1000000.0;
                    double freqWidthMHz = e.BwSupress;                    
                    ExternalExBearing2(freqMHz, freqWidthMHz);
                    break;
            }
        }
    }
}
