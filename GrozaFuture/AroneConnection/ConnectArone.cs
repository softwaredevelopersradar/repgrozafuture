﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;


namespace GrozaFuture
{
    public partial class MainWindow
    {       
        public int IzOpen { get; set; }
        public int BaudRate
        {
            get => basicProperties.Local.ARONE.PortSpeed;
            set { AroneConnetion.BaudRate = value; }
        }
        public string ComPort
        {
            get => basicProperties.Local.ARONE.ComPort;
            set { AroneConnetion.PortAdress = value; }

        }
        
        private void ARONEconnection_Click(object sender, RoutedEventArgs e)
        {
            AroneConnetion.PortAdress = ComPort;
            AroneConnetion.BaudRate = BaudRate;
            
            AroneConnetion.ConnectionToMain();
            AroneConnetion.OnComIsOpen += AroneConnetion_OnComIsOpen;
            AroneConnetion.ComIsOpen();
            if (IzOpen == 1)
            {
                ARONEconnection.ShowConnect();                
            }
            else { ARONEconnection.ShowDisconnect(); }
        }

        private void AroneConnetion_OnComIsOpen(object sender, WpfControlLibraryBut.ComIsOpenEventArgs e)
        {
            IzOpen = e.IsOpen;
        }
    }
}
