﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrozaFuture
{
    public partial class MainWindow
    {
        public int CurrentDataBank
        {
            get => basicProperties.Local.ARONE.CurrentDataBank;
            set { ReceiverTable.Bank = value; }
        }

        public void OnUpdateBank()
        {
            ReceiverTable.Bank = CurrentDataBank;
        }
    }
}
