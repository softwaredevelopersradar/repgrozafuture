﻿using AudioPlayerControl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace GrozaFuture
{
    public partial class MainWindow
    {        
        private AudioPlayer.APlayerWindow AudioPlayer;

        public void InitAPlayer()
        {
            AudioPlayer = new AudioPlayer.APlayerWindow();
        }

        private void bAudioPlayer_Click(object sender, RoutedEventArgs e)
        {
            if (AudioPlayer.IsVisible)
            {
                AudioPlayer.Hide();                
            }
                
            else
            {
                //if(APlayerControl.IsLoaded)
                AudioPlayer.Show();
                
            }
            
        }
    }
}
