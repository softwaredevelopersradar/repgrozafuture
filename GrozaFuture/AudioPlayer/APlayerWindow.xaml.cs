﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GrozaFuture.AudioPlayer
{
    /// <summary>
    /// Логика взаимодействия для APlayerWindow.xaml
    /// </summary>
    public partial class APlayerWindow : Window
    {
        public APlayerWindow()
        {
            InitializeComponent();
        }
        private void WAudioPlayer_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
            Visibility = Visibility.Hidden;
        }
    }
}
