﻿using System;
using Protocols;

namespace GrozaFuture
{
    public partial class MainWindow 
    {
        AWPtoDSPprotocolFuture dsp = new AWPtoDSPprotocolFuture();

        private bool isDspConnected = false;
        public bool IsDspConnected
        {
            get => isDspConnected;
            set
            {
                if (isDspConnected == value) return;
                isDspConnected = value;
                OnPropertyChanged();
            }
        }

        public void DispatchIfNecessary(Action action)
        {
            if (!Dispatcher.CheckAccess())
                Dispatcher.Invoke(action);
            else
                action.Invoke();
        }

        public void InitDSP()
        {
            dsp.ModeMessageUpdate += Dsp_ModeMessageUpdate;
            dsp.FiltersMessageUpdate += Dsp_FiltersMessageUpdate;
            dsp.RadioJamStateUpdate += Dsp_RadioJamStateUpdate;
            dsp.FhssRadioJamUpdate += Dsp_FhssRadioJamUpdate;
            dsp.StationLocationMessageUpdate += Dsp_StationLocationMessageUpdate;

            dsp.IsConnected += Dsp_IsConnected;
            dsp.IsRead += Dsp_IsRead;
            dsp.IsWrite += Dsp_IsWrite;

            dsp.ShaperStateUpdate += Dsp_ShaperStateUpdate;

            dsp.StationLocationMessageUpdate += Dsp_StationLocationMessageUpdate1;
            dsp.SetTimeUpdate += Dsp_SetTimeUpdate;
            dsp.NumberOfSatellitesUpdate += Dsp_NumberOfSatellitesUpdate;

            dsp.ShaperModeUpdate += Dsp_ShaperModeUpdate;
            dsp.ShaperVoltageUpdate += dsp_ShaperVoltageUpdate;
            dsp.ShaperPowerUpdate += dsp_ShaperPowerUpdate;
            dsp.ShaperTemperatureUpdate += dsp_ShaperTemperatureUpdate;
            dsp.ShaperAmperageUpdate += dsp_ShaperAmperageUpdate;
        }

        partial void Dsp_ModeMessageUpdate(Protocols.ModeMessage answer);

        partial void Dsp_FiltersMessageUpdate(FiltersMessage answer);

        partial void Dsp_RadioJamStateUpdate(RadioJamStateUpdateEvent answer);

        partial void Dsp_FhssRadioJamUpdate(FhssRadioJamUpdateEvent answer);

        partial void Dsp_StationLocationMessageUpdate(StationLocationMessage answer);

        partial void Dsp_IsConnected(bool isConnected);

        partial void Dsp_IsRead(bool isRead);

        partial void Dsp_IsWrite(bool isWrite);

        partial void Dsp_ShaperStateUpdate(ShaperStateUpdateEvent answer);

        partial void Dsp_StationLocationMessageUpdate1(StationLocationMessage answer);

        partial void Dsp_SetTimeUpdate(SetTimeRequest answer);

        partial void Dsp_NumberOfSatellitesUpdate(NumberOfSatellitesUpdateEvent answer);

        partial void Dsp_ShaperModeUpdate(ShaperConditionUpdateEvent answer);

        partial void dsp_ShaperVoltageUpdate(ShaperVoltageUpdateEvent answer);

        partial void dsp_ShaperPowerUpdate(ShaperPowerUpdateEvent answer);

        partial void dsp_ShaperTemperatureUpdate(ShaperConditionUpdateEvent answer);

        partial void dsp_ShaperAmperageUpdate(ShaperConditionUpdateEvent answer);
    }
}
