﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Protocols;

namespace GrozaFuture
{
    public partial class MainWindow
    {
        partial void Dsp_ModeMessageUpdate(Protocols.ModeMessage answer)
        {
            DispatchIfNecessary(() =>
            {
                switch (answer.Mode)
                {
                    case DspDataModel.Tasks.DspServerMode.Stop:
                        mPanel.Highlight = MainPanel.MPanel.Buttons.Preparation;
                        pLibrary.Mode = 0;
                        break;
                    case DspDataModel.Tasks.DspServerMode.RadioIntelligence:
                        mPanel.Highlight = MainPanel.MPanel.Buttons.RadioIntelligence;
                        pLibrary.Mode = 1;
                        break;
                    case DspDataModel.Tasks.DspServerMode.RadioIntelligenceWithDf:
                        mPanel.Highlight = MainPanel.MPanel.Buttons.RadioIntelligence;
                        pLibrary.Mode = 2;
                        break;
                    case DspDataModel.Tasks.DspServerMode.RadioJammingFrs:
                        mPanel.Highlight = MainPanel.MPanel.Buttons.RadioSuppression;
                        pLibrary.Mode = 3;
                        break;
                    case DspDataModel.Tasks.DspServerMode.RadioJammingAfrs:
                        mPanel.Highlight = MainPanel.MPanel.Buttons.RadioSuppression;
                        pLibrary.Mode = 4;
                        break;
                    case DspDataModel.Tasks.DspServerMode.RadioJammingVoice:
                        mPanel.Highlight = MainPanel.MPanel.Buttons.RadioSuppression;
                        pLibrary.Mode = 5;
                        break;
                    case DspDataModel.Tasks.DspServerMode.RadioJammingFhss:
                        mPanel.Highlight = MainPanel.MPanel.Buttons.RadioSuppression;
                        pLibrary.Mode = 6;
                        break;
                    case DspDataModel.Tasks.DspServerMode.Calibration:
                        mPanel.Highlight = MainPanel.MPanel.Buttons.Preparation;
                        pLibrary.Mode = 0;
                        break;
                }
            });
            AttCheck(answer.Mode);
        }

        partial void Dsp_FiltersMessageUpdate(FiltersMessage answer)
        {
            DispatchIfNecessary(() =>
            {
                pLibrary.Threshold = (answer.Threshold < 0) ? (-1) * answer.Threshold : answer.Threshold;
            });
        }

        partial void Dsp_RadioJamStateUpdate(RadioJamStateUpdateEvent answer)
        {
            DispatchIfNecessary(() =>
            {
                if (answer.TargetStates[0].RadioJamState == 1)
                {
                    double[] datax = new double[answer.TargetStates.Count()];
                    double[] datay = new double[answer.TargetStates.Count()];
                    for (int i = 0; i < answer.TargetStates.Count(); i++)
                    {
                        datax[i] = answer.TargetStates[i].Frequency / 10000d;
                        datay[i] = answer.TargetStates[i].Amplitude / -1d;
                    }
                    pLibrary.DrawRSArrows(true, datax, datay);
                }
                else
                    pLibrary.ClearArrowsAndLines();
            });
        }

        partial void Dsp_FhssRadioJamUpdate(FhssRadioJamUpdateEvent answer)
        {
            DispatchIfNecessary(() =>
            {
                pLibrary.IQDrawRSLines(answer.Frequencies);
            });
        }

        partial void Dsp_StationLocationMessageUpdate(StationLocationMessage answer)
        {

        }

        async public void DspVoiceEmit(bool value)
        {
            if (pLibrary.Mode == 5)
            {
                var answer = await dsp.SetVoiceJamming(value);
                if (value)
                {
                    pLibrary.DrawRSArrows(true, new[] { VoiceDisturb.Frequency }, new[] { -80d });
                }
                else
                {
                    pLibrary.ClearArrowsAndLines();
                }
            }
        }

    }
}
