﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Windows;
using DLL_Compass;
using ModelsTablesDBLib;

namespace GrozaFuture
{
    public partial class MainWindow
    {
        private Compass CompassRR;
        private Compass CompassPA;

        public double AngleRR
        {
            get
            {
                try
                {
                    if (CompassRR != null && IsConnectCmp[nameof(CompassRR)])
                    {
                        return CompassRR._angles.head;
                    }
                    return -1;
                }
                catch
                {
                    return -1;
                }
            }
        }

        public double AnglePA
        {
            get
            {
                try
                {
                    if (CompassPA != null && IsConnectCmp[nameof(CompassPA)])
                    {
                        return CompassPA._angles.head;
                    }
                    return -1;
                }
                catch
                {
                    return -1;
                }
            }
        }

        Dictionary<string, bool> IsConnectCmp = new Dictionary<string, bool>
        {
            {nameof(CompassRR), false },
            {nameof(CompassPA), false }
        };

        private class StandartParam
        {
            public static Parity Parity = Parity.None;
            public static byte DataBits = 8;
            public static StopBits StopBits = System.IO.Ports.StopBits.One;
        }

        private void InitCompass()
        {
            CompassRR = new Compass();
            CompassRR.OnConnectPort += CompassRR_Connect;
            CompassRR.OnDisconnectPort += CompassRR_Disconnect;
            CompassRR.OnReadByte += CompassRR_OnReadByte;
            CompassRR.OnWriteByte += CompassRR_OnWriteByte;
            CompassRR.OnMessage += CompassRR_OnMessage;

            CompassPA = new Compass();
            CompassPA.OnConnectPort += CompassPA_Connect;
            CompassPA.OnDisconnectPort += CompassPA_Disconnect;
            CompassPA.OnReadByte += CompassPA_OnReadByte;
            CompassPA.OnWriteByte += CompassPA_OnWriteByte;
            CompassPA.OnMessage += CompassPA_OnMessage;
        }

        #region Events CompassPA

        private void CompassPA_OnMessage(int type)
        {
            Dispatcher.Invoke(() =>
            {

            });
        }

        private void CompassPA_OnWriteByte(byte[] bByte)
        {
            this.Dispatcher.Invoke((Action)(() =>
            {
                CmpPAConnection.ShowWrite();
            }));
        }

        private void CompassPA_OnReadByte(byte[] bByte)
        {
            this.Dispatcher.Invoke((Action)(() =>
            {
                CmpPAConnection.ShowRead();
            }));
        }

        private void CompassPA_Disconnect()
        {
            this.Dispatcher.Invoke((Action)(() =>
            {
                IsConnectCmp[nameof(CompassPA)] = false;

                CmpPAConnection.ShowDisconnect();
            }));
        }

        private void CompassPA_Connect()
        {
            this.Dispatcher.Invoke((Action)(() =>
            {
                IsConnectCmp[nameof(CompassPA)] = true;

                CmpPAConnection.ShowConnect();

                SendToDbAnglePA();
            }));
        }

        private async void SendToDbAnglePA()
        {
            try
            {
                double angle;
                if (AnglePA == -1)
                {
                    angle = 0.0;
                }
                else
                {
                    angle = Math.Round(AnglePA, 1);
                    //angle = AnglePA - AnglePA % 0.1;
                }

                if (angle != -1)
                {
                    var tempGNSS = (await clientDB?.Tables[NameTable.TempGNSS].LoadAsync<TempGNSS>()).FirstOrDefault();
                    if (tempGNSS != null)
                    {
                        tempGNSS.CmpPA = angle;
                        clientDB.Tables[NameTable.TempGNSS].Change(tempGNSS);
                    }
                    else
                    {
                        clientDB?.Tables[NameTable.TempGNSS].Add(new TempGNSS() { CmpPA = angle });
                    }
                }
            }
            catch (Exception)
            { }
        }

        #endregion

        #region Methods CompassPA

        private void CmpPA_Click(object sender, RoutedEventArgs e)
        {
            if (!IsConnectCmp[nameof(CompassPA)])
                CompassPA.OpenPort(basicProperties.Local.CmpPA.ComPort, basicProperties.Local.CmpPA.PortSpeed, StandartParam.Parity, StandartParam.DataBits, StandartParam.StopBits);
            else
                CompassPA.ClosePort();
        }

        #endregion

        #region Events CompassRR

        private void CompassRR_OnMessage(int type)
        {
            this.Dispatcher.Invoke((Action)(() =>
            {
            }));
        }

        private void CompassRR_OnWriteByte(byte[] bByte)
        {
            this.Dispatcher.Invoke((Action)(() =>
            {
                CmpRRConnection.ShowWrite();
            }));
        }

        private void CompassRR_OnReadByte(byte[] bByte)
        {
            this.Dispatcher.Invoke((Action)(() =>
            {
                CmpRRConnection.ShowRead();
            }));
        }

        private void CompassRR_Disconnect()
        {
            this.Dispatcher.Invoke((Action)(() =>
            {
                IsConnectCmp[nameof(CompassRR)] = false;
                CmpRRConnection.ShowDisconnect();
            }));
        }

        private void CompassRR_Connect()
        {
            this.Dispatcher.Invoke(() =>
            {
                IsConnectCmp[nameof(CompassRR)] = true;
                CmpRRConnection.ShowConnect();

                SendToDbAngleRR();
            });

        }

        private async void SendToDbAngleRR()
        {
            try
            {
                double angle;
                if (AngleRR == -1)
                {
                    angle = 0.0;
                }
                else
                {
                    angle = Math.Round(AngleRR, 1);
                    //angle = AngleRR - AngleRR % 0.1;                    
                }

                if (angle != -1)
                {
                    var tempGNSS = (await clientDB?.Tables[NameTable.TempGNSS].LoadAsync<TempGNSS>()).FirstOrDefault();
                    if (tempGNSS != null)
                    {
                        tempGNSS.CmpRR = angle;
                        clientDB.Tables[NameTable.TempGNSS].Change(tempGNSS);
                    }
                    else
                    {
                        clientDB?.Tables[NameTable.TempGNSS].Add(new TempGNSS() { CmpRR = angle });
                    }
                }
                if (basicProperties.Global.SignHeadingAngle == true)
                {
                    GlobalProperties newGlobal = basicProperties.Global.Clone();
                    int tempangle = (int)(angle + 90);
                    if (tempangle >= 360) tempangle = tempangle - 360;
                    newGlobal.HeadingAngle = tempangle;
                    HandlerGlobalProperties(this, newGlobal);
                }
            }
            catch(Exception)
            { }
        }

        #endregion

        #region Methods CompassRR

        private void CmpRR_Click(object sender, RoutedEventArgs e)
        {
            if (!IsConnectCmp[nameof(CompassRR)])
                CompassRR.OpenPort(basicProperties.Local.CmpRR.ComPort, basicProperties.Local.CmpRR.PortSpeed, StandartParam.Parity, StandartParam.DataBits, StandartParam.StopBits);
            else
                CompassRR.ClosePort();
        }

        #endregion
    }
}
