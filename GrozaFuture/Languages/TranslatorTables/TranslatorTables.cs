﻿using ModelsTablesDBLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace GrozaFuture
{
    public partial class MainWindow : Window, INotifyPropertyChanged
    {

        private void SetLanguageTables(Languages language)
        {
            ResourceDictionary dict = new ResourceDictionary();
            try
            {
                switch (language)
                {
                    case Languages.Eng:
                        dict.Source = new Uri("/GrozaFuture;component/Languages/TranslatorTables/TranslatorTables.EN.xaml",
                                      UriKind.Relative);
                        break;
                    case Languages.Rus:
                        dict.Source = new Uri("/GrozaFuture;component/Languages/TranslatorTables/TranslatorTables.RU.xaml",
                                           UriKind.Relative);
                        break;
                    default:
                        dict.Source = new Uri("/GrozaFuture;component/Languages/TranslatorTables/TranslatorTables.RU.xaml",
                                          UriKind.Relative);
                        break;
                }

                this.Resources.MergedDictionaries.Add(dict);
            }
            catch (Exception ex)
            { }
        }

        private void UcSRangesRecon_OnIsWindowPropertyOpen(object sender, SectorsRangesControl.SectorsRangesProperty e)
        {
            e.SetLanguagePropertyGrid(basicProperties.Local.Common.Language);
        }

        private void UcSpecFreqKnown_OnIsWindowPropertyOpen(object sender, SpecFreqControl.SpecFreqProperty e)
        {
            e.SetLanguagePropertyGrid(basicProperties.Local.Common.Language);
        }

        private void UcSpecFreqImportant_OnIsWindowPropertyOpen(object sender, SpecFreqControl.SpecFreqProperty e)
        {
            e.SetLanguagePropertyGrid(basicProperties.Local.Common.Language);
        }

        private void UcSpecFreqForbidden_OnIsWindowPropertyOpen(object sender, SpecFreqControl.SpecFreqProperty e)
        {
            e.SetLanguagePropertyGrid(basicProperties.Local.Common.Language);
        }

        private void UcSRangesSuppr_OnIsWindowPropertyOpen(object sender, SectorsRangesControl.SectorsRangesProperty e)
        {
            e.SetLanguagePropertyGrid(basicProperties.Local.Common.Language);
        }


        private void UcASP_OnIsWindowPropertyOpen(object sender, ASPControl.ASPProperty e)
        {
            e.SetLanguagePropertyGrid(basicProperties.Local.Common.Language);
            
        }

    }
}
